package dao;

import entities.User;

import java.util.List;

public interface UserDAO {
    /**
     * create - добавляет нового пользователя в БД
     * @param user
     */
    boolean add(User user);

    // read
    User getByLogin(String login);

    User getById(Long id);

    List<User> getAll();

    // update
    boolean update(User user);

    // delete
    boolean remove(User user);
}
