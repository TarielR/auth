package dao;

import bl.HibernateUtil;
import entities.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class UserDAOImpl extends HibernateUtil implements UserDAO {
    @Override
    public boolean add(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        if (getByLogin(user.getLogin()) != null) {
            transaction.commit();
            session.close();
            return false;
        }

        session.save(user);

        transaction.commit();
        session.close();
        return true;
    }

    @Override
    public User getByLogin(String login) {
        String hqlQuery = "from User where login = ?";

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery(hqlQuery);
        query.setParameter(0, login);

        User user = (User) query.uniqueResult();

        transaction.commit();
        session.close();
        return user;
    }

    @Override
    public User getById(Long id) {
        String sqlQuery = "SELECT * FROM USERS WHERE ID = :param";

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createNativeQuery(sqlQuery).addEntity(User.class);
        query.setParameter("param", id);

        User user = (User) query.getSingleResult();

        transaction.commit();
        session.close();

        return user;
    }

    @Override
    public List<User> getAll() {
        String sqlQuery = "SELECT * FROM USERS";

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createNativeQuery(sqlQuery).addEntity(User.class);

        List<User> userList = query.list();

        transaction.commit();
        session.close();

        return userList;
    }

    @Override
    public boolean update(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        if (getByLogin(user.getLogin()) == null) {
            transaction.commit();
            session.close();
            return false;
        }

        session.update(user);

        transaction.commit();
        session.close();
        return true;
    }

    @Override
    public boolean remove(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        if (getByLogin(user.getLogin()) == null) {
            transaction.commit();
            session.close();
            return false;
        }

        session.remove(user);

        transaction.commit();
        session.close();
        return true;
    }
}
