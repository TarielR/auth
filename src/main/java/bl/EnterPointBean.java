package bl;


import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named (value = "enterPoint")
@SessionScoped
public class EnterPointBean implements Serializable {
    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    @Lock(LockType.WRITE)
    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    @Lock(LockType.WRITE)
    public void setPassword(String password) {
        this.password = password;
    }

    public void doLogin() {
        System.out.println("doLogin()");
    }
}
