import bl.HibernateUtil;
import dao.UserDAO;
import dao.UserDAOImpl;
import entities.User;

import java.util.List;

public class EnterPoint {

    public static void main(final String[] args) throws Exception {
        UserDAO userDAO = new UserDAOImpl();

        User user = userDAO.getByLogin("userTEST");
        userDAO.remove(user);

        List<User> users = userDAO.getAll();
        for (User u : users)
            System.out.println(u);




        HibernateUtil.shutdown();
    }
}