package dao;

import entities.User;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class UserDAOImplTest extends Assert {
    private static UserDAO userDAO;
    private static User user;

    @BeforeClass
    public static void setUp() throws Exception {
        userDAO = new UserDAOImpl();
        ///
        user = new User();
        //user.setId(100000L);
        user.setLogin("userTEST");
        user.setPass("pass01");
        user.setAdmin(Boolean.FALSE);
        user.setRegistrationDate(Timestamp.valueOf(LocalDateTime.now()));
        ///

    }


    @Test
    public void addTest() throws Exception {
        assertTrue(userDAO.add(user));
        assertFalse(userDAO.add(user));  // уже существует
    }

    @Test
    public void getByIdTest() throws Exception {
        assertNotNull(userDAO.getById(user.getId()));
//        assertNull(userDAO.getById(200000L));
    }

    @Test
    public void getAllTest() throws Exception {
        assertTrue(userDAO.getAll().size() > 0);
    }

    @Test
    public void remove() throws Exception {
        assertTrue(userDAO.remove(user));
        assertFalse(userDAO.remove(user)); // Уже удален
    }

}